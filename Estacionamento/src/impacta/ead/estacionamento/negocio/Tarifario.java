package impacta.ead.estacionamento.negocio;

/**
 * Representa dados de valores a serem cobrados pelo periodo de estacionamento;
 * 
 * @author Ricardo
 *
 */
public class Tarifario {
	public static int VALOR_HORA = 20;
	public static int VALOR_INCREMENTAL = 2;
	public static int INCREMENTO_MINUTOS = 15;

}
