package impacta.ead.estacionamento.negocio;

public class Vaga {
	public static int TOTAL_VAGAS = 100;

	private static int vagasOcupadas = 0;

	private Vaga() {
	}

	public static boolean temVagaLivre() {
		// TODO implementar este metodo.
		return false;
	}

	public static void inicializarOcupadas() {
		// TODO implementar
	}

	public static int ocupadas() {
		return Vaga.vagasOcupadas;
	}

	public int livres() {
		return TOTAL_VAGAS - Vaga.vagasOcupadas;
	}

	public static void entrou() {
		Vaga.vagasOcupadas++;
	}
}
